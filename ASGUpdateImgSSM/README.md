﻿# Create IAM role
## Specification
**Name:** LambdaAMIPatching
**Service:** Lambda
**Policies:**
 - AWSLambdaFullAccess
 - AutoScalingFullAccess
 - AmazonSSMAutomationRole

**Trusted relationships:**
```json
ssm.amazonaws.com,
ec2.amazonaws.com,
lambda.amazonaws.com
```
## Guide
 - [ ] TODO: Translate to english

1.	Vybrat modul IAM pro vytvoreni role
2.	V levem menu kliknout na polozku Roles a pote vybrat Create New Role
3.	Vybrat moznost Lambda a kliknout na Next:Permissions 
4.	Vybrat AWSLambdaFullAccess, AutoScalingFullAccess, AmazonSSMAutomationRole pote kliknout na Next:Tags
5.	Zde vyplne Vami zvolene tagy a kliknete na Next:Review
6.	Zvolte nazev role a vyplnte jej do Role name  
7.	V seznamu vyhledejte vami vytvorenou roli a kliknete na jeji nazev napr. LambdaAMIPatching
8.	Pote v detailu vyberte zalozku **Trusted relationships** a pote na **Edit trust relationship** 
9.	Text zmente podle specifikaci
10. Kliknete na Update trust policy

# Lambda functions

## Automation-UpdateSsmParam
**Name:**  Automation-UpdateSsmParam
**IAM role:** LambdaAMIPatching
**Runtime:** Python 3.x

## Automation-UpdateAsg
**Name:**  Automation-UpdateAsg
**IAM role:** LambdaAMIPatching
**Runtime:** Python 3.x

## Automation-AmiCleanUp
**Name:**  Automation-AmiCleanUp
**IAM role:** LambdaAMIPatching
**Runtime:** Python 3.x

## Update image automation
Set **assumeRole** value to your *LambdaAMIPatching* role!
