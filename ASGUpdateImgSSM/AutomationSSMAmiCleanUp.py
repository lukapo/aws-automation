import json
import boto3
from datetime import datetime, timedelta
from botocore.exceptions import ClientError


def lambda_handler(event, context):
    print("Received event: " + json.dumps(event, indent=2))

    appName=event['appname']
    days_count=int(event['thresholdDays'])
    ami_age_limit = datetime.now() - timedelta(days=days_count)
    
    ec2_client = boto3.client('ec2')
    as_client = boto3.client('autoscaling')

    images_list = ec2_client.describe_images(Filters=[{'Name': 'tag:AppName', 'Values': [appName]} ])
    lc_list = as_client.describe_launch_configurations()
    cleanup_counter = 0
    
    # AMI Cleanup
    for image in images_list['Images']:
        image_id = image['ImageId']
        date_string = image['CreationDate']
        image_dt = datetime.strptime(date_string, '%Y-%m-%dT%H:%M:%S.%fZ')
        if image_dt < ami_age_limit:
            instances_list = ec2_client.describe_instances(
                Filters=[{'Name': 'image-id', 'Values': [image_id]}]
            )
            active_reservations = instances_list['Reservations']
            if not active_reservations:
                # There might be a client error if we run int multiple times
                try:
                    ec2_client.deregister_image(ImageId=image_id)
                except ClientError:
                    pass
                print('Image %s was removed' % image_id)
                cleanup_counter += 1
                
            # Launch configuration cleanup    
            for lc in lc_list['LaunchConfigurations']:
                lc_imageid = lc['ImageId']
                if lc_imageid == image_id:
                    try:
                        as_client.delete_launch_configuration(LaunchConfigurationName=lc['LaunchConfigurationName'])
                    except ClientError:
                        pass
                    print('Launch Configuration %s was removed' % lc['LaunchConfigurationName'])
                    
            # Snapshot cleanup
            snapshot_list = ec2_client.describe_snapshots(Filters=[{'Name': 'description', 'Values': ['*'+image_id+'*']} ])
            for snapshot in snapshot_list['Snapshots']:
                if image_id in snapshot['Description']:
                    try:
                        ec2_client.delete_snapshot(SnapshotId=snapshot['SnapshotId'])
                    except ClientError:
                        pass
                    print('Snapshot %s was removed' % snapshot['SnapshotId'])
                
    
    if cleanup_counter:
        print('Totally %s images have been cleaned up.' % cleanup_counter)
    else:
        print('There is no images to be removed.')