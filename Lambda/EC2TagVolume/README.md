# EC2 volumes tag script
Copy tags from EC2 instance to attached volume

## Prerequisities
 - create IAM role for lambda
 - set lambda environment variables

## Create IAM role
1. Select IAM module
2. Click on "Create new role"
3. Choose lambda and click to "Next:Permissions"
4. Select AmazonEC2FullAccess 
5. Click on Next:Review
6. Fill role name and finish

## Create Lambda function
1. Select Lambda module
2. Click on Create function.
3. Select option "Author from scratch" fill function name "EC2TagVolume". 
4. Select runtime - Python 3.7 ( latest )
5. Select "Use an existing role" and choose role from previous step. 
6. Click on "Create function"
7. Upload zip file with function and libraries

## Set environments variables
### deleteVolumeTag
Set to True if you want delete tags, which are not set in instance. 