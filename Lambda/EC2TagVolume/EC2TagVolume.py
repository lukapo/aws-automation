import json
import boto3
import os
from botocore.exceptions import ClientError

# Declare clients
ec2_client = boto3.client("ec2")
try:
    deleteVolumeTag = str(os.environ["deleteVolumeTag"]).lower()
except:
    deleteVolumeTag = "false"

def getAllInstancesTags(ec2_client):
    try:
        Dict = {}
        responseDict = ec2_client.describe_instances(
            Filters=[{
                'Name': 'instance-state-name',
                'Values': ['stopped', 'running']
                }]
        )
        for instances in responseDict["Reservations"]:
            for instance in instances['Instances']:
                Dict[instance['InstanceId']] = {}
                for tag in instance['Tags']:
                    if "aws:" not in tag['Key']:
                        Dict[instance['InstanceId']][tag['Key']] = tag['Value']
        return Dict
    except ClientError as e:
        print(e.response['Error']['Code'])
        pass

def getAllVolumesTags(ec2_client):
    try:
        Dict = {}
        responseDict = ec2_client.describe_volumes(
            Filters=[{
                'Name': 'attachment.status',
                'Values': ['attached']
                }]
        )
        for volumes in responseDict["Volumes"]:
            for attachement in volumes['Attachments']:
                Dict[volumes['VolumeId']] = {}
                Dict[volumes['VolumeId']]['InstanceId'] = attachement['InstanceId']
                Dict[volumes['VolumeId']]['Tags'] = {}
                try:
                    for tag in volumes['Tags']:
                        if "aws:" not in tag['Key']:
                            Dict[volumes['VolumeId']]['Tags'][tag['Key']] = tag['Value']
                except KeyError:
                    pass

        return Dict
    except ClientError as e:
        print(e.response['Error']['Code'])
        pass

def addVolumeeTag(client, volumeId, tagName, tagValue):
    try:
        client.create_tags(
            Resources=[
                volumeId,
            ],
            Tags=[{
                'Key': tagName,
                'Value': tagValue
            }]
        )
    except ClientError as e:
        print(e.response['Error']['Code'])
        pass

def delVolumeTag(client, volumeId, tagName):
    try:
        client.delete_tags(
            Resources=[
                volumeId,
            ],
            Tags=[{
                'Key': tagName
            }]
        )
    except ClientError as e:
        print(e.response['Error']['Code'])
        pass

def main():
    instances = getAllInstancesTags(ec2_client)
    volumes = (getAllVolumesTags(ec2_client))
    for volume in volumes.keys():
        for tag in instances[volumes[volume]['InstanceId']].keys():
            try:
                if tag not in volumes[volume]['Tags'].keys():
                    addVolumeeTag(ec2_client, volume, tag, instances[volumes[volume]['InstanceId']][tag])
                    print("Tag %s chybi u volume %s - přidávám tag u volume %s!" % (tag, volume, volume))
            except KeyError:
                addVolumeeTag(ec2_client, volume, tag, instances[volumes[volume]['InstanceId']][tag])
                print("Tag %s chybi u volume %s" % (tag, volume))
        
        if deleteVolumeTag == "true": 
            for tag in volumes[volume]['Tags'].keys():
                if tag not in instances[volumes[volume]['InstanceId']].keys():
                    delVolumeTag(ec2_client, volume, tag)
                    print("Tag %s je u volume %s navic - mažu tag u volume %s!" % (tag, volume, volume))

def lambda_handler(event, context):
    main()