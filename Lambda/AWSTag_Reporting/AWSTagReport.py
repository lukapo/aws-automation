import io
import csv
import json
import boto3
from datetime import datetime
from botocore.exceptions import ClientError

# Version
version = "16102019"

# Variables
resourceDict={} 
s3Bucket="lamdba-tag-report"
topicSns = 'arn:aws:sns:'
rolesArn = {
            'arn:aws:iam:dev:':'dev',
            'arn:aws:iam:preprod:':'preprod'
            }

def aws_session(role_arn=None, session_name='my_session'):
    """
    Create session to AWS API

    @param role_arn 
    @param session_name 
    @return aws session
    """
    if role_arn:
        client = boto3.client('sts')
        response = client.assume_role(RoleArn=role_arn, RoleSessionName=session_name)
        session = boto3.Session(
            aws_access_key_id=response['Credentials']['AccessKeyId'],
            aws_secret_access_key=response['Credentials']['SecretAccessKey'],
            aws_session_token=response['Credentials']['SessionToken'])
        return session
    else:
        return boto3.Session()

def genFieldNames(resource_dict):
    """
    Generate fieldnames for csv

    @param resource_dict - returned dict from AWS API
    @return list - list of columns for csv
    """
    tag_list=["ID", "ResourceType"]
    for tag in resource_dict["Tags"]:
        if tag["Key"] not in tag_list:
            tag_list.append(tag["Key"])
    return tag_list

def export_to_S3(s3, s3Bucket, context, fieldnames, today, role_arn_env):
    """
    Save file to S3

    @param s3 - s3 boto3 client
    @param s3Bucket - bucket name in S3
    @param context - data for csv export type dict
    @param fieldnames - list of columns in csv
    @param toady - date 
    @param role_arn_env - environment name
    """
    filename = "tmp/"+today+"/_"+role_arn_env+"_tagging_report.csv"
    temporary_field = io.StringIO()
    writer = csv.DictWriter(temporary_field, fieldnames=fieldnames)
    writer.writeheader()
    for resource in resourceDict[role_arn_env]:
        for tag in fieldnames:
            if tag not in resourceDict[role_arn_env][resource].keys():
                resourceDict[role_arn_env][resource][tag]="NULL"
        writer.writerow(resourceDict[role_arn_env][resource])
    s3.Object(s3Bucket, filename).put(Body=temporary_field.getvalue())

def lambda_handler(event, context):
    print("Running Tag reporter %s - version %s" % (datetime.today(), version))
    today = datetime.now().strftime("%Y_%M_%d")
    for key, value in rolesArn.items():
        role_arn=key
        role_arn_env=value
        resourceDict[role_arn_env] = {}
        session_assumed = aws_session(role_arn=role_arn, session_name='my_lambda')
        session_regular = aws_session()
        ec2_client = session_assumed.client('ec2')
        s3_client = session_regular.resource('s3')
        sns_client = session_regular.client('sns')
    
        tag_report = ec2_client.describe_tags()

        for tag in tag_report["Tags"]:
            if tag['ResourceId'] not in resourceDict[role_arn_env].keys():
                resourceDict[role_arn_env][tag['ResourceId']]={}
                resourceDict[role_arn_env][tag['ResourceId']]["ID"]=tag['ResourceId']
                resourceDict[role_arn_env][tag['ResourceId']][tag["Key"]]=tag["Value"]
                resourceDict[role_arn_env][tag['ResourceId']]["ResourceType"]=tag["ResourceType"]
            else:
                resourceDict[role_arn_env][tag['ResourceId']][tag["Key"]]=tag["Value"]
        tag_list=genFieldNames(tag_report)
        export_to_S3(s3_client, s3Bucket, resourceDict[role_arn_env], tag_list, today, role_arn_env)

    sns_client.publish(
        TopicArn = topicSns,
        Message="latest export available here: https://s3.console.aws.amazon.com/s3/buckets/"+s3Bucket+"/"+today+"/?region=us-east-1&tab=overview",
        Subject='Tagging report '+today+' available.',
        MessageAttributes={}
    )